<?php

Route::group(['prefix' => 'v1', 'as' => 'admin.', 'namespace' => 'Api\V1\Admin',  'middleware' => ['swfix']], function () {
    Route::apiResource('users', 'UsersApiController');

//    Route::apiResource('products', 'ProductsApiController');

    Route::apiResource('students', 'StudentsApiController');
});
