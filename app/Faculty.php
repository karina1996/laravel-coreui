<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Faculty extends Model
{
    /**
     * The courses that belong to the faculty.
     */
    public function course()
    {
        return $this->belongsToMany(Course::class);
    }

    /**
     * Get the group for the student.
     */
    public function students()
    {
        return $this->belongsToMany(Student::Class);
    }
}
