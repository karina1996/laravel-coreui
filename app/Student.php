<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'group_id',
        'faculty_id',
        'name',
        'surname',
        'email',
        'phone',
        'created_at',
        'updated_at'
    ];

    /**
     * Get the group for the student.
     */

    public function group()
    {
        return $this->belongsTo(Group::Class);
    }

    /**
     * Get the group for the student.
     */
    public function faculty()
    {
        return $this->belongsTo(Faculty::Class);
    }
}
