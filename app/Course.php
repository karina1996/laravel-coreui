<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * Get the groups for the course.
     */
    public function groups()
    {
        return $this->hasMany(Group::class);
    }

    /**
     * Get the students for the group.
     */
    public function students()
    {
        return $this->hasManyThrough(Student::class, Group::class);
    }
}
