<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use Illuminate\Http\Request;
use App\Group;
use App\Course;
use App\Faculty;
use App\Student;

class StudentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $faculties = Faculty::all();

        return view('admin.students.index', compact('faculties'));
    }

    public function studentFilter(Request $request)
    {
        $students = Student::with(['faculty', 'group'])->get();

        if(!empty($request->input('faculty'))) {
            $students = Student::where('faculty_id', $request->input('faculty'))->get();
        }

        if(!empty($request->input('course'))) {
            $students = Course::where('course_number', $request->input('course'))->firstOrFail()->students;
        }

        if(!empty($request->input('group'))) {
            $students = Group::where('group_number', $request->input('group'))->firstOrFail()->students;
        }

        $datatable = datatables()->of($students)
            ->editColumn('id', '')
            ->setRowAttr([
                'data-entry-id' => function($student) {
                    return $student->id;
                },
            ])
            ->editColumn('faculty', function ($student) {
                return !empty($student->faculty->name) ? $student->faculty->name : '';
            })
            ->editColumn('course', function ($student) {
                return !empty($student->group->course->course_number) ? $student->group->course->course_number : '';
            })
            ->editColumn('group', function ($student) {
                return !empty($student->group->group_number) ? $student->group->group_number : '';
            })
            ->addColumn('action', function ($student) {
                return '<a href="' . route('admin.students.edit', $student->id) . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
            })
            ->setRowId('id');


        return $datatable->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
       $faculties = Faculty::all();
       $courses   = Course::all();
       $groups    = Group::all();

        return view('admin.students.create', compact('faculties', 'courses', 'groups'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreStudentRequest $request)
    {
        Student::create($request->all());

        return redirect()->route('admin.students.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */

    public function show(Student $student)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */

    public function edit(Student $student)
    {
        $faculties = Faculty::all();
        $courses   = Course::all();
        $groups    = Group::all();

        return view('admin.students.edit', compact('student', 'faculties', 'courses', 'groups'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */

    public function update(UpdateStudentRequest $request, Student $student)
    {
        $student->update($request->all());

        return redirect()->route('admin.students.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Student  $student
     * @return \Illuminate\Http\Response
     */

    public function destroy(Student $student)
    {
        //
    }
}
