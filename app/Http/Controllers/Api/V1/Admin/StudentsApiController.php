<?php

namespace App\Http\Controllers\Api\V1\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Student;

class StudentsApiController extends Controller
{
    /**
     * @SWG\Get(
     *     path="/api/v1/students",
     *     summary="Get list of students",
     *     tags={"Students"},
     *     security={{"MyHeaderAuthentication":{}}},
     *     @SWG\Response(
     *         response=200,
     *         description="successful operation",
     *     ),
     *     @SWG\Response(
     *         response="401",
     *         description="Unauthorized user",
     *     )
     * )
     */

    public function index()
    {
        $students = Student::all();

        return $students;
    }

    /**
     * @SWG\Post(
     *     path="/api/v1/students",
     *     description="Return a student's name, surname, email, phone, faculty, group",
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         type="string",
     *         description="Student name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="surname",
     *         in="query",
     *         type="string",
     *         description="Student surname"
     *     ),
     *      @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         type="string",
     *         description="Student email"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="query",
     *         type="string",
     *         description="Student phone"
     *     ),
     *     @SWG\Parameter(
     *         name="faculty_id",
     *         in="query",
     *         type="integer",
     *         description="Student faculty id"
     *     ),
     *     @SWG\Parameter(
     *         name="group_id",
     *         in="query",
     *         type="integer",
     *         description="Student group id"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */

    public function store(StoreStudentRequest $request)
    {
        $student = Student::create($request->all());

        return $student;

    }

    /**
     * @SWG\Get(
     *      path="/api/v1/students/{id}",
     *      operationId="getStudentById",
     *      tags={"Students"},
     *      security={{"MyHeaderAuthentication":{}}},
     *      summary="Get studnet information",
     *      description="Returns student data",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Student id",
     *         required=true,
     *     ),
     *      @SWG\Response(
     *          response=200,
     *          description="Successful operation"
     *       ),
     *      @SWG\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @SWG\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @SWG\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */

    public function show(Student $student)
    {
        return $student;
    }

    /**
     * @SWG\Put(
     *      path="/api/v1/students/{id}",
     *      operationId="updateStudent",
     *      tags={"Students"},
     *      consumes={"application/x-www-form-urlencoded"},
     *      produces={"application/json"},
     *      summary="Update existing student data",
     *      description="Returns updated student data",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Student id",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="name",
     *         in="query",
     *         type="string",
     *         description="Student name",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="surname",
     *         in="query",
     *         type="string",
     *         description="Student surname"
     *     ),
     *      @SWG\Parameter(
     *         name="email",
     *         in="query",
     *         type="string",
     *         description="Student email"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="query",
     *         type="string",
     *         description="Student phone"
     *     ),
     *     @SWG\Parameter(
     *         name="faculty_id",
     *         in="query",
     *         type="integer",
     *         description="Student faculty id"
     *     ),
     *     @SWG\Parameter(
     *         name="group_id",
     *         in="query",
     *         type="integer",
     *         description="Student group id"
     *     ),
     *      @SWG\Response(
     *          response=202,
     *          description="Successful operation",
     *       ),
     *      @SWG\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *      @SWG\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @SWG\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @SWG\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */

    public function update(UpdateStudentRequest $request, Student $student)
    {
        return $student->update($request->all());
    }

    /**
     * @SWG\Delete(
     *     path="/api/v1/students/{id}",
     *     tags={"Student"},
     *     summary="Delete existing student",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Student id",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Successful operation"
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     ),
     *      @SWG\Response(
     *          response=404,
     *          description="Resource Not Found"
     *      )
     * )
     */

    public function destroy(Student $student)
    {
        return $student->delete();
    }
}
