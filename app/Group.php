<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{

    /**
     * Get the students for the group.
     */
    public function students()
    {
        return $this->hasMany(Student::Class);
    }

    /**
     * Get the students for the group.
     */
    public function course()
    {
        return $this->belongsTo(Course::Class);
    }
}
