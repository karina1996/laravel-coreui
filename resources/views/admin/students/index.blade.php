@extends('layouts.admin')
@section('content')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" href="{{ route("admin.students.create") }}">
                    {{ trans('global.add') }} {{ trans('global.student.title_singular') }}
                </a>
            </div>
        </div>
    <div class="card">
        <div class="card-header">
            {{ trans('global.student.title_singular') }} {{ trans('global.list') }}
        </div>

        <div class="card-body">
            <div class="table-responsive">
                <div id="student-datatable-filter" class="panel panel-default">
                    <div class="panel-body">
                        <form method="POST" id="search-form" class="form-inline" role="form">

                            <div class="form-group filter_type">
                                <label for="filter_type">{{ trans('global.datatables.filter_type') }}</label>
                                <select name="filter_type" id="filter_type" class="form-control">
                                    <option value="">{{ trans('global.datatables.filter_type') }}</option>
                                    <option value="faculty">{{ trans('global.student.fields.faculty') }}</option>
                                    <option value="course">{{ trans('global.student.fields.course') }}</option>
                                    <option value="group">{{ trans('global.student.fields.group') }}</option>
                                </select>
                            </div>

                            <div class="form-group" id="filter-faculty-section">
                                <label for="faculty_filter">{{ trans('global.datatables.faculty') }}</label>
                                <select name="faculty_filter" id="faculty_filter" class="form-control">
                                    <option value="">{{ trans('global.student.fields.faculty') }}</option>
                                    @foreach($faculties as $faculty)
                                        <option value="{{ $faculty->id }}">{{ $faculty->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group" id="filter-course-section">
                                <label for="course_filter">{{ trans('global.datatables.course') }}</label>
                                <input type="text" class="form-control" name="course" id="course_filter" placeholder="{{ trans('global.datatables.course_search') }}">
                            </div>
                            <div class="form-group" id="filter-group-section">
                                <label for="group_filter">{{ trans('global.datatables.group') }}</label>
                                <input type="text" class="form-control" name="group" id="group_filter" placeholder="{{ trans('global.datatables.group_search') }}">
                            </div>
                            <div id="submit-filter" class="form-group">
                                <button type="submit" class="btn btn-primary">Search</button>
                            </div>
                        </form>
                    </div>
                </div>
                <table id="student-datatable" class=" table table-bordered table-striped table-hover datatable">
                    <thead>
                    <tr>
                        <th>

                        </th>
                        <th>
                            {{ trans('global.student.fields.name') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.surname') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.email') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.phone') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.faculty') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.course') }}
                        </th>
                        <th>
                            {{ trans('global.student.fields.group') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
@section('scripts')
    @parent
    <script>
        $(function () {
           let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)

            $('#student-datatable').DataTable({
                searching: true,
               buttons: dtButtons,
                processing: true,
                serverSide: true,
                ajax: {
                    headers: {'x-csrf-token': _token},
                    type: 'POST',
                    url:"{{ route('admin.students.filtered') }}",
                    data: function (d) {
                        d.faculty = $('#faculty_filter').val();
                        d.course = $('#course_filter').val();
                        d.group = $('#group_filter').val();
                    }
                },
                columns: [
                    { data: 'id', name: 'id', orderable: false, searchable: false, printable: false},
                    { data: 'name', name: 'name' },
                    { data: 'surname', name: 'surname' },
                    { data: 'email', name: 'email' },
                    { data: 'phone', name: 'phone' },
                    { data: 'faculty', name: 'faculty' },
                    { data: 'course', name: 'course' },
                    { data: 'group', name: 'group' },
                    { data: 'action', name: 'action', orderable: false, searchable: false, printable: false },
                ],
                order: [[0, 'desc']]

            });

            $("#search-form").on("submit", function(event) {
                event.preventDefault();

                $('#student-datatable').DataTable().draw(true);
            });

            $("#filter_type").on("change", function() {
                var filter = $(this).val();
                var activeFilter = '#filter-' + filter + '-section';

                $(this).parents('form').find('div').removeClass('active');
                $(this).parents('form').find('div:not(.filter_type) input, div:not(.filter_type) select').val('');

                if(filter !== '') {
                    $(activeFilter).addClass('active');
                    $("#submit-filter").addClass('active');
                } else {
                    $("#submit-filter").removeClass('active');
                    $('#student-datatable').DataTable().draw(true);
                }
            });
        })

    </script>
@endsection
@endsection
