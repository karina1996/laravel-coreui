@extends('layouts.admin')
@section('content')

    <div class="card">
        <div class="card-header">
            {{ trans('global.edit') }} {{ trans('global.student.title_singular') }}
        </div>

        <div class="card-body">
            <form action="{{ route("admin.students.update", [$student->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.student.fields.name') }}*</label>
                    <input type="text" id="name" name="name" class="form-control" value="{{ old('name', isset($student) ? $student->name : '') }}">
                    @if($errors->has('name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.student.fields.name_helper') }}
                    </p>
                </div>
                <div class="form-group {{ $errors->has('surname') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.student.fields.surname') }}*</label>
                    <input type="text" id="surname" name="surname" class="form-control" value="{{ old('name', isset($student) ? $student->surname : '') }}">
                    @if($errors->has('surname'))
                        <em class="invalid-feedback">
                            {{ $errors->first('surname') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.student.fields.surname_helper') }}
                    </p>
                </div>
                <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.student.fields.email') }}*</label>
                    <input type="text" id="email" name="email" class="form-control" value="{{ old('email', isset($student) ? $student->email : '') }}">
                    @if($errors->has('email'))
                        <em class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.student.fields.email_helper') }}
                    </p>
                </div>
                <div class="form-group {{ $errors->has('phone') ? 'has-error' : '' }}">
                    <label for="name">{{ trans('global.student.fields.phone') }}*</label>
                    <input type="text" id="phone" name="phone" class="form-control" value="{{ old('phone', isset($student) ? $student->phone : '') }}">
                    @if($errors->has('phone'))
                        <em class="invalid-feedback">
                            {{ $errors->first('phone') }}
                        </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('global.student.fields.phone_helper') }}
                    </p>
                </div>
                <div class="form-group filter_type">
                    <label for="faculty_id">{{ trans('global.student.fields.faculty') }}</label>
                    <select name="faculty_id" id="faculty_id" class="form-control">
                        {{--                        <option value="" disabled>{{ trans('global.student.fields.faculty') }}</option>--}}
                        @foreach($faculties as $faculty)
                            <option value="{{ $faculty->id }}" <?=$faculty['id'] == $student->faculty_id ? 'selected' : '';?>>{{ $faculty->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group filter_type">
                    <label for="course_id">{{ trans('global.student.fields.course') }}</label>
                    <select name="course_id" id="course_id" class="form-control">
                        {{--                        <option value="" disabled>{{ trans('global.student.fields.course') }}</option>--}}
                        @foreach($courses as $course)
                            <option value="{{ $course->id }}">{{ $course->course_number }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group filter_type">
                    <label for="group_id">{{ trans('global.student.fields.group') }}</label>
                    <select name="group_id" id="group_id" class="form-control">
                        @foreach($groups as $group)
                            <option value="{{ $group->id }}" <?=$group['id'] == $student->group_id ? 'selected' : '';?>>{{ $group->group_number }}</option>
                        @endforeach
                    </select>
                </div>
                <div>
                    <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
                </div>
            </form>
        </div>
    </div>

@endsection
