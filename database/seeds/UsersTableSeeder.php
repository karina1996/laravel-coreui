<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $users = [[
            'id'             => 1,
            'name'           => 'Admin',
            'email'          => 'admin@admin.com',
            'password'       => '$2y$10$UEpW0PzyDcfkDVjbF0YFqeJ4mGMdKaGpxfH2ylWEtnCercMWgdeBG',
            'remember_token' => null,
            'created_at'     => '2020-02-10 19:13:32',
            'updated_at'     => '2019-02-10 19:13:32',
            'deleted_at'     => null,
        ]];

        User::insert($users);
    }
}
