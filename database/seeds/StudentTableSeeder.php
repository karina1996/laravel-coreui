<?php

use Illuminate\Database\Seeder;
use App\Faculty;
use App\Group;
use App\Student;

class StudentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->delete();

        $faker = Faker\Factory::create();

        foreach(range(1, 10) as $index)
        {
            $group   = Group::orderByRaw("RAND()")->first();
            $faculty = Faculty::orderByRaw("RAND()")->first();

            Student::create([
                'group_id'   => $group->id,
                'faculty_id' => $faculty->id,
                'name'       => $faker->firstName,
                'surname'    => $faker->lastName,
                'email'	     => $faker->unique()->email,
                'phone'	     => $faker->phoneNumber,
                'created_at' => $faker->dateTime($max = 'now')
            ]);
        }
    }
}
