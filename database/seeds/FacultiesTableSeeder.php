<?php

use Illuminate\Database\Seeder;
use App\Faculty;

class FacultiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('faculties')->delete();

        $faculties_data = [
            [
                'name'      => 'History',
                'created_at' => '2019-04-15 19:13:32'
            ],
            [
                'name'      => 'Armenian Philology',
                'created_at' => '2019-04-15 19:13:32'
            ],
            [
                'name'      => 'Chemistry',
                'created_at' => '2019-04-15 19:13:32'
            ],
            [
                'name'      => 'Physics',
                'created_at' => '2019-04-15 19:13:32'
            ],
//            [
//                'name'      => 'Economics and Management',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Mathematics and Mechanics',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Biology',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Geography and Geology',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Law',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Russian Philology',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Oriental Studies',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Journalism',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Philosophy and Psychology',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Radiophysics',
//                'created_at' => '2019-04-15 19:13:32'
//            ],
//            [
//                'name'      => 'Sociology',
//                'created_at' => '2019-04-15 19:13:32'
//            ]
        ];

        Faculty::insert($faculties_data);
    }
}
