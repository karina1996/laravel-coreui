<?php

use Illuminate\Database\Seeder;
use App\Group;
use App\Course;

class GroupsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();

        $faker = Faker\Factory::create();

        foreach(range(1, 5) as $index)
        {
            $course = Course::orderByRaw("RAND()")->first();

            Group::create([
                'course_id' 	=> $course->id,
                'group_number'  => $faker->numberBetween(100,200),
                'created_at'	=> $faker->dateTime($max = 'now')
            ]);
        }
    }
}
