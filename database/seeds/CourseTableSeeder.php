<?php

use Illuminate\Database\Seeder;
use App\Course;

class CourseTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->delete();
        $faker = Faker\Factory::create();


        for ($i = 1; $i < 6; $i++)
        {
            $courses = Course::create(array(
                'course_number' => $i,
            ));
        }
    }
}
