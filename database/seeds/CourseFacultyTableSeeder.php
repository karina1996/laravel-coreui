<?php

use Illuminate\Database\Seeder;

class CourseFacultyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_faculty')->delete();

        $courseIds  = DB::table('courses')->pluck('id')->all();
        $facultyIds = DB::table('faculties')->pluck('id')->all();

        //Seed user_role table with 20 entries
        foreach ((range(1, 30)) as $index)
        {
            DB::table('course_faculty')->insert(
                [
                    'course_id' => $courseIds[array_rand($courseIds)],
                    'faculty_id' => $facultyIds[array_rand($facultyIds)]
                ]
            );
        }
    }
}
