<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            PermissionRoleTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            FacultiesTableSeeder::class,
            CourseTableSeeder::class,
            GroupsTableSeeder::class,
            CourseFacultyTableSeeder::class,
            StudentTableSeeder::class,
        ]);
    }
}
